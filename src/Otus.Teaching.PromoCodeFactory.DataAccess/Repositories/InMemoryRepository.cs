﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }
        private static IEnumerable<T> SavedData { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            if (SavedData == null)
            {
                Data = data;
                return;
            }
            
            Data = SavedData;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> PostAsync(T t)
        {
            Data = Data.Append<T>(t);

            SaveChanges();
            
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == t.Id));
        }

        public Task<T> PutAsync(T t)
        {
            Data = Data.Where(x => x.Id != t.Id).Append(t);
            
            SaveChanges();
            
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == t.Id));
        }

        public Task<Guid> DeleteAsync(Guid id)
        {
            Data = Data.Where(x => x.Id != id);
            
            SaveChanges();

            return Task.FromResult(id);
        }

        private void SaveChanges()
        {
            var baseEntities = Data as T[] ?? Data.ToArray();
            var saved = new T[baseEntities.Length];
            baseEntities.ToArray().CopyTo(saved, 0);
            SavedData = saved;
        }
    }
}