﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        [HttpPost]
        public async Task<EmployeeShortResponse> CreateEmployeesAsync(string firstName, string lastName, string email,
            string roles, int promoCount)
        {
            var rolesIds = roles.Split(',');
            var rolesList = new List<Role>();

            foreach (var roleId in rolesIds)
            {
                var rolesController = new RolesController(new InMemoryRepository<Role>(FakeDataFactory.Roles));
                try
                {
                    var res = await rolesController.GetRoleByIdAsync(new Guid(roleId));
                    rolesList.Add(new Role()
                    {
                        Id = res.Value.Id,
                        Name = res.Value.Name,
                        Description = res.Value.Description
                    });
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
            
            var employee = new Employee()
            {
                Id = Guid.NewGuid(),
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Roles = rolesList,
                AppliedPromocodesCount = promoCount
            };

            await _employeeRepository.PostAsync(employee);
            
            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName
            };
        }
        
        [HttpPut ("ChangeEmail")]
        public async Task<ActionResult<EmployeeShortResponse>> PutEmployeesAsync(Guid id, string email)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            employee.Email = email;

            await _employeeRepository.PutAsync(employee);

            return new EmployeeShortResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                FullName = employee.FullName
            };
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteEmployeesAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);

            return Ok();
        }
    }
}